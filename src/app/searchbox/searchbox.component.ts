import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {
  @Input() label: string;
  @Output() onSearch = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onSearchButtonClicked() {
    this.onSearch.emit();
  }

}
