import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  labelText = 'Search for items';

  handleSearch() {
    console.log('Search was clicked on app-searchbar');
  }
}
